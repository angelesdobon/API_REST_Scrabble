# FASE 1. PLANIFICACIÓN PROYECTO SCRABBLE

## 1.0 Apuntes de entrega

Se establecen con el cliente las siguientes premisas:
1. Una vez el usuario se conecta puede elegir entre:
    * Jugar partida: Recibirá, de la máquina o de otro usuario, invitaciones para jugar.
    * Retar jugador: Podrá elegir usuario al que enviar invitación de jugar partida.
2. Cada usuario sólo puede llevar en marcha un máximo de 3 partidas (siempre que haya suficientes usuarios disponibles, es decir, conectados y con menos de 3 partidas en marcha). Esta premisa facilita la fluidez del juego evitando la espera durante la participación del contrincante.
3. Las partidas terminan cuando:
    * Las mismas, finalizan normalmente según las reglas
    * Cualquiera de los jugadores abandona voluntariamente    
4. Cuando un usuario lleva en marcha menos de 3 partidas el sistema le propone nuevas partidas:
    * Mientras existan usuarios conectados jugando menos de 3 partidas a la vez.
    * Hasta que el usuario acepte el acceso a 3 partidas.
    * Hasta que el usuario exprese su negativa a recibir más sugerencias de partida. Así, queda prevista la opción de que el usuario sólo quiera jugar una partida a la vez y no reciba notificaciones, automáticas o de otros usuarios, que lo importunen.

Tras investigar diversas plataformas de juego, “¡Scrabble Go!”, aplicación para móviles, es la que me ha parecido más intuitiva y fiel al juego original. Me he basado en ella para tomar ciertas decisiones a la hora de crear el Diagrama de flujo y para definir ciertas partes de los modelos alámbricos de nuestro proyecto.

![Sin título](img/Tablero_ScrabbleGo.png)

## 1.1 Estudio de usuarios

**Pablo**, es un estudiante de 3º de ESO de un colegio de Valencia. Sus actividades favoritas son, leer y jugar a videojuegos (por supuesto en el orden inverso). Sus asignaturas favoritas son inglés y lengua, las matemáticas se le dan fatal. En general es un niño responsable y feliz. Está acostumbrado a usar su ordenador para jugar y hacer deberes. También usa el móvil de su abuela, que es demasiado buena y se lo deja siempre, para navegar y jugar.

Hoy hay reunión familiar, pero no vienen sus primos de Alicante, así que, solo hay mayores y llega un momento en que se aburre. Le coge el móvil a su abuela, y busca si existe algún sitio web donde jugar al Scrabble. La última vez que vinieron los primos se lo trajeron y a lo mejor puede jugar sólo de alguna manera, pero no puede instalar nada en el dispositivo, que luego le caen broncas. Busca Scrabble en Español online, y encuentra la plataforma “¡Un mundo de letras!” creada por CEED “Centro de específico de educación a distancia” donde se puede jugar. ”¡Qué bien!” Se registra con la cuenta de correo que sus padres le supervisan (y tiene para este tipo de cosas) y empieza a jugar. La tarde no será tan larga.

**Ana**, es estudiante a distancia de Bachillerato. Sus padres tienen un bar y desde que tiene 16 años ha preferido ayudarles en él antes que estudiar. Sus padres no quieren que deje los estudios por lo que le obligan a ir sacando el Bachillerato a distancia, aunque sea a su ritmo. El trato es media jornada de trabajo para ganar dinero, media jornada estudiar, lo consideran una solución para no cerrarse puertas. Ana es muy sociable y le interesan muy poco los ordenadores, hay uno en casa para todos, y ella lo utiliza para usar las redes sociales, seguir “influencers” de moda y cuando no hay más remedio para hacer los deberes.
Hoy es día de deberes, cómo siempre, ha ido retrasando el momento y es tarde, pero tiene que hacer el trabajo de lengua que le han mandado en el CEED, su centro de educación a distancia. Le han propuesto varios sitios donde investigar y aprender sobre la invención de la Imprenta, uno de los sitios es la plataforma “¡Un mundo de letras!” de su propio centro, empieza por ahí, espera sacar rápidamente la información necesaria para poder volver a chatear con sus amigos en Face-Book.

**Manuela**, está jubilada, es viuda, y aunque pasa mucho tiempo con sus hijos, nueras y nietos desde que está viuda siente que le sobra demasiado tiempo. Le preocupa su pérdida de memoria y se ha apuntado a clases de “Memorización” en el hogar del jubilado. Allí le han recomendado mantener la mente activa haciendo crucigramas, sudokus y jugando en general. Sus hijos le regalaron hace tiempo una Tablet y se defiende para usar el Whats-App (desde casa con el wifi), ver videos de Youtube de Jota Aragonesa y navegar por internet. Esto último no lo controla demasiado y sus hijos le riñen porque, sin querer, acaba instalando mucho “maleguare”, ha creído entender, y a saber qué es eso.

Una aburrida tarde de jueves, (nadie ha venido hoy a casa a comer), se pone a navegar en búsqueda de algún juego que la entretenga y le sirva para conservar la memoria. Entre muchos
sitios que no entiende, encuentra “¡Un mundo de letras!”, parece interesante, pero ¿Porqué, puede ver las reglas del juego y luego no puede entrar a jugar? No entiende nada, se parece a un juego de su nieta Carmen, pero “¿Usuario? ¿Clave?”, dejará la web en la Tablet para preguntarle a algún nieto cuando venga a comer. “¡Cómo se ha pasado el rato!, normal que los críos estén enganchados, voy a poner el hervido que se hace tarde”.

**Paco**, 47 años, trabaja de administrativo en Autoridad Portuaria. Le encantan los móviles, los ordenadores y cualquier dispositivo con el que conectarse a internet, posiblemente demasiado. Si fuese más joven hubiera estudiado informática, pero es su momento, Contabilidad le parecía más lógico. A veces su mujer le llama la atención para que deje el móvil, sobre todo en la mesa, pues es mal ejemplo para sus hijos. Sabe que tiene razón, pero no lo puede evitar.

Hoy viaja en avión con su mujer, de escapada a Florencia. Lleva su Tablet, ¿cómo no?, para ver una peli en el trayecto, pero su mujer, ¿cómo no?, se ha dormido. Se pone a navegar sin un interés concreto, pero cae en una aplicación web de juegos, parece, “¡Un mundo de letras!”. Estudia brevemente la posibilidad de jugar al Scrabble, y piensa si vale la pena registrarse para ello, ¿Lo hará?...

## 1.2 Mapa Conceptual

![Mapa Conceptual](img/02_MapaConceptual.svg "Mapa Conceptual")

## 1.3 Mapa del sitio web

![Mapa del Sitio Web](img/03_MapaSitioWeb.svg "Mapa del Sitio Web")

## 1.4 Diagramas de flujo

Se compone de 6 diagramas, uno general para el flujo de la partida y 5 más para definir distintas partes del proceso.

* [00_General_partida](diagramas_flujo/04_00_DF_general_partida.pdf "00_General_partida")
* [01_Ordenar jugadores](diagramas_flujo/04_01_DF_ordenar_jugadores.pdf "01_Ordenar jugadores")
* [02_Turno jugador](diagramas_flujo/04_02_DF_turno_jugador.pdf "02_Turno jugador")
* [03_Recepcion de fichas](diagramas_flujo/04_03_DF_recepcion_fichas.pdf "03_Recepcion de fichas")
* [04_Introduccion palabra](diagramas_flujo/04_04_DF_introduccion_palabra.pdf "04_Introduccion palabra")
* [05_Cambio de letras](diagramas_flujo/04_05_DF_cambio_letras.pdf "05_Cambio de letras")

## 1.5 Modelos Alámbricos

**Imprenta**
![Imprenta](img/modelos_alambricos/05_01_M_A_Imprenta.jpg "Imprenta")

**Informacion Scrabble**
![Información Scrabble](img/modelos_alambricos/05_02_M_A_InformacionScrabble.jpg "Información Scrabble")

**Dashboard Usuario**
![Dashboard Usuario](img/modelos_alambricos/05_03_M_A_DashboardUsuario.jpg "Dashboard Usuario")

**Tablero de juego**
![Tablero de Juego](img/modelos_alambricos/05_04_M_A_TableroJuego.jpg "Tablero de Juego")

