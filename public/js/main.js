/* eslint-disable no-undef */
$(document).ready(function(){
    
    //Tooltip
    $('a[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'top',
        /* html: true */
    });   

    //Menú desplegable sección imprenta    
    $('#imprentaBoton').click(function(){
        //boton abrir y cerrar menú (rallitas y cruz)
        $('#menuImprenta').toggle();
        $('.men-menu').toggle();
        $('.men-cross').toggle();        
    
    });
    
    //Funcionamiento menú resposive area imprenta
    $('#menuImprenta .nav-link').click(function(){

        //Cambio por apartados en la URL
        window.location.href = "/" + $(this).attr("href"); 
        
        //Cierre del menú con cada selección en pantallas pequeñas
        var x = window.matchMedia("(max-width: 992px)");
        if (x.matches) { // If media query matches
            $('#menuImprenta').toggle();
            $('.men-menu').toggle();
            $('.men-cross').toggle();
        }
    });

    // Initate masonry grid ¿Por qué si no redimensiono la web no va?
    //Funcionamiento menú resposive area imprenta
    $('#menuImprenta .nav-link:last-child').click(function(){
        var $grid = $('.gallery-wrapper').masonry({
            temSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
        });
    
        // Initate imagesLoaded
        $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
        });        
    });    

    //Arrow animation basada en: https://codepen.io/xzf/pen/BvGLjL 

    //Arrow trabajos
    var $arrow1 = document.querySelector('.icon_arrow .arrow');    
    $arrow1.animate([
        {left: '0'},
        {left: '50px'},
        {left: '0'}
    ],{
        duration: 2000,
        iterations: Infinity
    });

    //Arrow libros
    var $arrow2 = document.querySelector('.icon_arrow2 .arrow');    
    $arrow2.animate([
        {left: '0'},
        {left: '50px'},
        {left: '0'}
    ],{
        duration: 2000,
        iterations: Infinity
    });

    //Arrow inventoA
    var $arrow3 = document.querySelector('.icon_arrow3 .arrow');    
    $arrow3.animate([
        {left: '0'},
        {left: '50px'},
        {left: '0'}
    ],{
        duration: 2000,
        iterations: Infinity
    });

    //Arrow inventoB
    var $arrow4 = document.querySelector('.icon_arrow4 .arrow');    
    $arrow4.animate([
        {left: '0'},
        {left: '50px'},
        {left: '0'}
    ],{
        duration: 2000,
        iterations: Infinity
    });

    //Carrousel lugares        

    $('.grid-item').click(function(){
        //que ninún indicador o item esté activo
        $(".carousel-indicators li").removeClass("active");
        $(".carousel-item").removeClass("active");

        //necesitamos saber la posición del grid item que se selecciona
        var index = $(this).index();
        
        var indicador =".carousel-indicators li:nth-child(" + index + ")";
        var item = ".carousel-item:nth-child(" + index + ")";

        //Usamos dicha posición para añadir la clase active al indicador y el item correspondiente del carrusel
        $(indicador).addClass("active");
        $(item).addClass("active");
       
    });
    
        
})

