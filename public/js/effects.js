$(document).ready(function(){

  //Efecto estrellas tintineantes en botones principales

  var botonesShine = document.getElementsByClassName("shines");
  var loop = {
  //initilizeing
  start: function() {

  for(var j = 0; j < botonesShine.length; j++){
    var anchoBoton = botonesShine[j].clientWidth;
    for (var i = 0; i <= Math.round(anchoBoton/30); i++) {
      var star = this.newStar();
      star.style.top = this.rand() * 100 + '%';
      star.style.left = this.rand() * 100 + '%';
      star.style.webkitAnimationDelay = this.rand() + 's';
      star.style.mozAnimationDelay = this.rand() + 's';
      botonesShine[j].appendChild(star);
    }
  }

  },
  //to get random number
  rand: function() {
    return Math.random();
    
  },
  //createing html dom for star
  newStar: function() {
    var d = document.createElement('div');
    d.innerHTML =
      '<figure class="star"><figure class="star-top"></figure><figure class="star-bottom"></figure></figure>';
    return d.firstChild;
  },
};

loop.start(); 

}
)