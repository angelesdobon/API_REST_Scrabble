<!-- TARJETAS BASE CON AMPLIACIÓN DE IMÁGEN-->
<div id="tarjeta_base" class="tab-pane">
<!-- Gallery item -->
    <div class="col-lg-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#tr1" data-toggle="collapse" aria-expanded="false" aria-controls="collapsetr1">            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/trabajos_01_a_thumbnail.png" alt="">
                </div>
            </div>
            <div class="p-4 contenido">
                <div class="number"><span>1</span></div>
                <h5 class="text-dark"> Componedor </h5>
                <p class="text-muted mb-0">Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una a una, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita, confecciona toda una página.</p>            
            </div>
        </div>
        <!-- tr1 imagen entera -->
        <div id="tr1" class="collapse fade big-image">
            <div>
                <div class="card">
                    <div class="card-body">
                        El componedor
                        <button type="button" data-toggle="collapse" data-target="#tr1" aria-label="Close" class="close ml-2 mb-1">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <img class="card-img-bottom" src="img/trabajos_01_a.png" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

<!-- Texto item -->
    <div class="col-lg-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">
                <p class="text-muted mb-0">El primer libro impreso en España fue el El sinodal de Aguilafuente en 1472 en Segovia.</p>
                <h5 class="text-dark">Valencia</h5>
                <p class="text-muted mb-0">Los tres primeros impresos en València con el procedimiento de Gutenberg fueron...</p>
            </div>
            <!-- Flecha -->
            <div class="icon_arrow">
                <div class="arrow"></div>
            </div>
        </div>
    </div>
    <!-- End --> 
</div>