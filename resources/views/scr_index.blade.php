@extends('layouts.app',
    ['title' => 'Info Scrabble', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontDashboard', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects'], 
    'js_files' => ['test_scr_index' , 'main', 'effects']])

    @section('content')
        <!-- <aside class="sidebar">
            <ul>
                <li class="input-menu"><a href="{{ route('register') }}">Registro</a></li>
                <li class="input-menu upper-margin"><a href="{{ route('login') }}">Login</a></li>
                <li class="input-menu upper-margin"><a v-on:click.prevent="ranking" href="#">Ranking usuarios</a></li>
                <li class="input-menu"><a v-on:click.prevent="currentGames" href="#">Últimas partidas en juego</a></li>
                <li class="input-menu"><a v-on:click.prevent="generalInfo" href="#">Información sobre el sistema</a></li>
            </ul>
        </aside>        
        <card-container-component :cards="c_cards"></card-container-component> -->

        <!-- borrado en css files: test_scr_index -->

        <!-- CABECERA DEL CONTENIDO -->
        <header class="row"> 
                <h1 class="col-md-12">¡Un mundo de letras!</h1>
                <h2 class="col-md-6">El Scrabble Online del CeedCV</h2>
                <form class="form-inline col-md-6 d-flex justify-content-md-end">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button"><span class="das-search"></span></button>
                    </div>
                </div>
            </form>                    
        </header>

        <!-- CARDS CON GIRO -->
        <article id="info" class="row d-flex justify-content-stretch container pr-0 pl-4">
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card-g tarjeta">
                    <div class="face front-face shadow-sm rounded">
                        <img src="img/info_01.jpg" class="cover" alt="imagen de bola del mundo con ojos" title="Foto de Anna Shvets en Pexels">
                    </div>
                    <div class="face back-face shadow-sm rounded contenedor p-3">
                    <h4 class="mb-3">¡Juega con {{ $numberUsers }} jugadores de todas las partes del mundo!</h4>
                        <button type="button" class="btn fondo_explosion2">
                            <a href="#modal_retar" data-toggle="modal" class="nav-link">Rétalos</a>
                        </button>                        
                    </div>                        
                </div>
            </div>           
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card-g tarjeta w-100">
                    <div class="face front-face shadow-sm rounded">
                        <img src="img/info_03.jpg" class="cover" alt="foto de 3 amigas riendo en el cesped" title="Foto de JESSICA TICOZZELLI en Pexels">
                    </div>
                    <div class="face back-face shadow-sm rounded contenido p-3">
                        <h4 class="mb-3">Comparte en las redes para jugar con tus amigos.</h4>
                        <button type="button" class="btn fondo_explosion2">
                            <a href="#modal_compartir" data-toggle="modal" class="nav-link">¡Comparte!</a>
                        </button>                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4">            
                <div class="card shadow-sm tarjeta h-100 no_pointer flex-column justify-content-end">            
                    <div class="p-4 contenido">
                        <h4 class="azul mx-0 my-3">¿Aún no estás jugando Scrabble con nosotros?</h4>
                        <h4 class="azul mx-0 my-3">¡Vamos!</h4>                           
                        <button type="button" class="btn fondo_explosion1 shines w-100">
                            <a href="http://localhost/scrabble/register" class="nav-link text-light">¡REGÍSTRATE!</a>             
                        </button>                                                    
                    </div>                          
                </div>
            </div>  
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card-g tarjeta">
                    <div class="face front-face shadow-sm rounded">
                        <img src="img/info_04.jpg" class="cover" alt="imagen de tipos metálicos para imprenta antigua">
                    </div>
                    <div class="face back-face shadow-sm rounded contenido p-3">                        
                    <h4 class="mb-3">Le debemos mucho a Gutemberg, ¡Aprende más sobre el inventor de la imprenta!</h4>
                        <button type="button" class="btn fondo_explosion2">
                            <a href="./" class="nav-link">Guttemberg</a>
                        </button>
                    </div>
                </div>
            </div>
            

            <!-- VENTANAS MODALES -->
            <!-- Ventana modal sin maquetar RETAR-->
            <div class="modal" id="modal_retar" tabindex="-1" role="dialog" aria-labelledby="Retar" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_retar">¡Rétalos!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Este botón nos llevaría a una modal que mostraría los jugadores a los que se puede retar, pero para acceder a ellos el usuario deberá estar logeado.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                    </div>
                </div>
            </div>
            <!-- Ventana modal sin maquetar COMPARTIR-->
            <div class="modal" id="modal_compartir" tabindex="-1" role="dialog" aria-labelledby="Compartir" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_compartir">¡Comparte!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Este botón nos llevaría a una modal que mostraría las redes sociales en las que podemos compartir. No haría falta estar logueado para compartir.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                    </div>
                </div>
            </div> 
        </article>        
        @include('reglasScrabble')
              
        
    @endsection
    
