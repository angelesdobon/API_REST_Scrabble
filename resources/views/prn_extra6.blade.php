@extends('layouts.app',
    ['title' => 'Info Imprenta', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects'], 
    'js_files' => ['main', 'effects']])

@section('content')

    <!-- borrado en css files: test_welcome -->
    
    <!-- CRÉDITOS DEL SITIO WEB  -->
    <div class="form row d-flex justify-content-center">
        <div class="my-5 mx-3">
            <h2>API REST Scrabble</h2>
            <p>
                <a  target="_blank"href="https://perso.crans.org/besson/LICENSE.html"><img src="https://img.shields.io/badge/License-GPLv3-blue.svg" alt=""></a>
            </p>            
            <h3>Introducción</h3>
            <p>El objetivo del proyecto es crear una API REST que proporcione funcionalidades para jugar al <em>Scrabble</em> (Apalabrados) de manera remota. Esta API tiene únicamente fines educativos y se desarrolla para que sea utilizada por los alumnos del módulo de Diseño de Interfaces Web con el objeto de diseñar la parte cliente.</p>
            <h3>Herramientas</h3>
            <p>La API está desarrollada con <a  target="_blank"href="https://laravel.com/">Laravel</a>. Como SGBD se utiliza MySQL. Además se utilizan: <a  target="_blank"href="https://www.docker.com/">Docker</a>, para la gestión y uso de contenedores que faciliten la instalación y despliegue de la aplicación.</p>
            <h3>Imágenes y otros contenidos de terceros utilizados</h3>
            <h4>Imágenes Sección Imprenta</h4>
            <ul class="nav flex-column mx-4 my-2">
                <li>
                    <a  target="_blank"href="https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg">https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg</a>
                </li>
                <li>
                    <a  target="_blank"href="https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png">https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png</a>
                </li>
                <li>
                    <a  target="_blank"href="https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg">https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg</a>
                </li>
                <li>
                    <a  target="_blank"href="http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf">http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf</a>
                </li>
                <li>
                    <a  target="_blank"href="https://pixabay.com/es/lavado-gancho-de-ángulo-705674/">https://pixabay.com/es/lavado-gancho-de-ángulo-705674/</a>
                </li>
                <li>
                    <a  target="_blank"href="https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._Réplica..png">https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._Réplica..png</a>
                </li>
                <li>
                    <a  target="_blank"href="https://pixabay.com/es/fuente-juego-de-plomo-letras-1889146/">https://pixabay.com/es/fuente-juego-de-plomo-letras-1889146/</a>
                </li>
                <li>
                    <a  target="_blank"href="https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg">https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg</a>
                </li>
                <li>
                    <a  target="_blank"href="https://de.wikipedia.org/wiki/Datei:Press1520.png">https://de.wikipedia.org/wiki/Datei:Press1520.png</a>
                </li>
                <li>
                    <a  target="_blank"href="https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg">https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg</a>
                </li>
                <li>
                    <a  target="_blank"href="https://de.wikipedia.org/wiki/Datei:Press1520.png">https://de.wikipedia.org/wiki/Datei:Press1520.png</a>
                </li>
                <li>
                    <a  target="_blank"href="https://pixabay.com/es/europa-pa%C3%ADses-mapa-3d-rojo-rosa-151589/">https://pixabay.com/es/europa-pa%C3%ADses-mapa-3d-rojo-rosa-151589/</a>
                </li>
                <li>
                    <a  target="_blank"href="http://bvpb.mcu.es/es/consulta/registro.cmd?id=405381">http://bvpb.mcu.es/es/consulta/registro.cmd?id=405381</a>
                </li>
                <li>
                    <a  target="_blank"href="http://bdh-rd.bne.es/viewer.vm?id=0000177130&page=1">http://bdh-rd.bne.es/viewer.vm?id=0000177130&page=1</a>
                </li>
                <li>
                    <a  target="_blank"href="https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG">https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG</a>
                </li>
                <li>
                    <a  target="_blank"href="https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg">https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg</a>
                </li>
                <li>
                    <a  target="_blank"href="https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg">https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg</a>
                </li>
                <li>Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación.</li>
                <li>Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación</li>
                <li>Calle San Vicente numero 3 de València. Foto de José M. Marín. Con autorización de publicación</li>
                <li>Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación</li>
            </ul>
            <h4>Imágenes Sección Scrabble Info</h4>
            <ul class="nav flex-column mx-4 my-2">
                <li>
                    <a  target="_blank"href="https://www.pexels.com/es-es/foto/naturaleza-vacaciones-verano-sol-4342428/">Foto de JESSICA TICOZZELLI en Pexels</a>
                </li>
                <li>
                    <a  target="_blank"href="https://www.pexels.com/es-es/foto/123-vamos-a-ir-texto-imaginario-704767/">Foto de SevenStorm JUHASZIMRUS en Pexels</a>
                </li>
                <li>
                    <a  target="_blank"href="https://www.pexels.com/es-es/foto/naturaleza-tierra-modelo-danar-5217883/">Foto de Anna Shvets en Pexels</a>
                </li>
                <li>
                    <a  target="_blank"href="https://www.pexels.com/es-es/license/">www.pexels.com</a>
                </li>
            </ul>
            <h3>Efectos especiales adaptados</h3>
            <ul class="nav flex-column mx-4 my-2">
                <li>Código de Origen NEON<br>
                    <a  target="_blank"href="#">Lo he perdido y no lo encuentro</a>
                </li>

                <li>Código de Origen ESTRELLAS DESTELLEANTES (SHINES)<br>
                    <a  target="_blank"href="https://www.techumber.com/amazing-glitter-star-effect-using-pure-css3">https://www.techumber.com/amazing-glitter-star-effect-using-pure-css3</a>
                </li>
                
                <li>Animación Arrow<br>
                    <a  target="_blank"href="https://codepen.io/xzf/pen/BvGLjL">https://codepen.io/xzf/pen/BvGLjL</a>
                </li>
            </ul>
            <h3>Snippets de bootstrap adaptados</h3>
            <ul class="nav flex-column mx-4 my-2">
                <li>Tooltips Guttemberg<br>
                    <a  target="_blank"href="https://bbbootstrap.com/snippets/tooltip-image-and-content-36745684">https://bbbootstrap.com/snippets/tooltip-image-and-content-36745684</a>
                </li>

                <li>Maquetación básica de cards con imágenes<br>
                    <a  target="_blank"href="https://bootstrapious.com/p/bootstrap-photo-gallery">https://bootstrapious.com/p/bootstrap-photo-gallery</a>
                </li>
                <li>Galería mashonry<br>
                    <a  target="_blank"href="https://bootstrapious.com/p/bootstrap-masonry">https://bootstrapious.com/p/bootstrap-masonry</a>
                </li>

                <li>Cards giratorios<br>
                    <a  target="_blank"href="https://bbbootstrap.com/snippets/bootstrap-5-user-s-testimonial-card-flipping-and-background-animation-90081105">https://bbbootstrap.com/snippets/bootstrap-5-user-s-testimonial-card-flipping-and-background-animation-90081105</a>
                </li>            
            </ul>
            <h3>Autores</h3>
            <ul class="nav flex-column mx-4 my-2">
                <li>Alfredo Oltra</li>
                <li>Ángeles Dobón</li>
            </ul>
            <h3>Licencia</h3>
            <p>El proyecto está liberado bajo licencia <a  target="_blank"href="https://www.gnu.org/licenses/gpl-3.0-standalone.html)">GPL 3.0</a></p>

        </div>
    </div>
@endsection