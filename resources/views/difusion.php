<!-- DIFUSIÓN INFO -->
<div id="difusion" class="tab-pane">
    <h2>Difusión de la idea</h2>
    <div class="row px-4">
        <div class="col-lg-8 rounded bg-white d-flex align-items-center">
            <img src="img/difusion_01.jpg" alt="Retrato de Guttemberg" class="w-100 rounded">
        </div>
        <div class="col-lg-4">
            <p>La idea de Gutenberg de las letras individuales grabadas en metal se difunde por Europa. Comienza en MAINZ (hacia 1450), van apareciendo las principales ciudades en donde se instalan imprentas:</p>
            <ul class="list-group">
                <li class="list-group-item">1469 Venecia (Italia)</li>
                <li class="list-group-item">1470 París (Francia)</li>
                <li class="list-group-item">1471 Brujas (Holanda)</li>
                <li class="list-group-item">1472 Segovia (España)</li>
                <li class="list-group-item">1474 Valencia (España)</li>
                <li class="list-group-item">1476 Westminster (Gran Bretaña)</li>
            </ul>                              
        </div>
    </div> 
</div>