@extends('layouts.app', 
    ['title' => 'Reinicio contraseña', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble' ,'styleSpecialEffects'], 
    'js_files' => ['test_scr_reset', 'effects']])

@section('content')

    @include('cabecera_scrabble')
    
    <!-- MOSTRAMOS ERRORES SI EXISTEN -->
    @if ($errors->isNotEmpty())
        <div class="error  row d-flex justify-content-center">
            <div class="col-md-12">
                <div>
                    <!-- <h4>Error modo 1</h4> -->
                    <ul class="list-group mt-4">
                        @foreach ($errors->all() as $error)
                            <li class="list-group-item list-group-item-warning alert alert-dismissible mt-2" role="alert">{{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- <div>
                <h4>Error modo 2</h4>
                @if ($errors->has('email'))
                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                @endif
            </div> -->
        </div>
    @endif
    @if (session('status'))
        <div class="informacion">
            {{ session('status') }}       
        </div>
    @endif

    <!-- CARDS CON INFO LOGIN Y FORMULARIO CORRESPONDIENTE -->
    <div class="form row d-flex justify-content-center flex-column-reverse flex-md-row mt-5">
        @include('info_register&login')
        <!-- card de reinicio de contraseña -->
        <div class="col-md-6 mb-4">            
            <div id="login" class="card shadow-sm tarjeta h-100 no_pointer">            
                <div class="p-4 contenido">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf <!-- por razones educativas está desactivado -->

                        <input  class="form-control  my-2" id="email" type="email" name="email" required placeholder="email">
                        <br> 
                        <button class="btn fondo_explosion1 w-100 p-0 mb-3" type="submit">                    
                            <span class="nav-link text-light"> Envia enlace para el reinicio de contraseña</span>
                        </button>
                        <div class="letra_pequena mt-3">                           
                            <p class="mt-1 mb-0">¿Ya estás registrado? <a href="{{ route('login') }}">Login</a></p>
                            <p class="mt-1 mb-0">¿No tienes cuenta? <a href="{{ route('register') }}">Crea una</a></p>
                        </div> 
                    </form>
                </div>
            </div>
        </div>      
    </div>    
    
@endsection
