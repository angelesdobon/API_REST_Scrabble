@extends('layouts.app', 
    ['title' => 'Login', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble' ,'styleSpecialEffects'], 
    'js_files' => ['test_scr_login', 'effects']])

@section('content')

    <!-- borrado en css files: test_scr_login -->

    <!-- <aside class="sidebar">
        <ul>
            <li class="input-menu"><a href="{{ route('register') }}">Registro</a></li>
            <li class="input-menu upper-margin"><a v-on:click="automatic_login1" href="#">Login (user1)</a></li>
            <li class="input-menu" id="b02"><a v-on:click="automatic_login2" href="#">Login (user2)</a></li>
            <li class="input-menu" id="b03"><a v-on:click="wrong_login" href="#">Login erróneo</a></li>
            <li class="input-menu upper-margin" id="b04"><a href="{{ route('password.request') }}">Contraseña olvidada</a></li>
        </ul>
    </aside> -->   

    
    @include('cabecera_scrabble')
    
    <!-- MOSTRAMOS ERRORES SI EXISTEN -->
    @if ($errors->isNotEmpty())
    <div class="error row d-flex justify-content-center">
        <div class="col-md-12">     
            <div>
                <!-- <h4>Error modo 1</h4> -->
                <ul class="list-group mt-4">
                    @foreach ($errors->all() as $error)
                        <li class="list-group-item list-group-item-warning alert alert-dismissible mt-2" role="alert">{{ $error }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </li>
                    @endforeach
                </ul>                
            </div>
            <!-- <div>
                <h4>Error modo 2</h4>
                @if ($errors->has('email'))
                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                @endif
            </div> -->
        </div>
    </div>
    @endif

    <!-- CARDS CON INFO LOGIN Y FORMULARIO CORRESPONDIENTE -->
    <div class="form row d-flex justify-content-center flex-column-reverse flex-md-row  mt-5">
        @include('info_register&login')
        <!-- card de login -->
        <div class="col-md-6 mb-4">            
            <div id="login" class="card shadow-sm tarjeta h-100 no_pointer">            
                <div class="p-4 contenido">                
                    <form class="form-group mb-0" method="POST" action="{{ route('login') }}">
                        @csrf <!-- por razones educativas está desactivado -->                    
                        <input id="email" class="form-control my-2" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="email">
                        <br>                    
                        <input id="password" class="form-control mb-2" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="password">
                        <br> 
                        <button class="btn fondo_explosion1 shines w-100 p-0 mb-3" type="submit">                    
                            <span class="nav-link text-light">LOGIN</span>
                        </button>
                        <div class="letra_pequena mt-3">                           
                            <a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                            <p class="mt-1 mb-0">¿No tienes cuenta? <a href="{{ route('register') }}">Crea una</a></p>
                        </div>               
                    </form>                                                    
                </div>                          
            </div>
        </div>
    </div>
    
@endsection
