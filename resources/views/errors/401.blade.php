@extends('layouts.app',
['title' => 'Error 401', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects', 'styleErrorPages'], 
'js_files' => ['test_scr_index' , 'main', 'effects']])

@section('content')

<!-- CABECERA ERRORES -->
<header class="row"> 
    <h1 class="col-md-12">Scrabble</h1>
    <h2 class="col-md-6">Error 401</h2>
    <form class="form-inline col-md-6 d-flex justify-content-md-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button"><span>lupa</span></button>
            </div>
        </div>
    </form>                    
</header>
<!-- <p>No está autorizado para ver esta página. Prueba a loguearte</p> -->
<div class="row m-4">
    <div class="col-md-4 text-right">
        <h1 class=" azul">¡Ooops!</h1>
        <h5>No está autorizado <br> para ver esta página.<br>
        Logueate.</h5>
    </div>
    <div class="col-md-8 mt-2">
    <div class="mensaje">
        <span class="char p1">N</span>            
        <span class="char p1">O</span>
        <span class="vacio"></span>
    
        <span class="char p2">E</span>
        <span class="char p2">S</span>
        <span class="char p2">T</span>
        <span class="char p2">A</span>
        <span class="char p2">S</span>           
        <span class="vacio"></span>

        <span class="char p3">A</span>
        <span class="char p3">U</span>
        <span class="char p3">T</span>
        <span class="char p3">O</span>
        <span class="char p3">R</span>
        <span class="char p3">I</span>
        <span class="char p3">Z</span>            
        <span class="char p3">A</span>
        <span class="char p3">D</span>
        <span class="char p3">O</span>          

        <span class="char p4">P</span>
        <span class="char p4">A</span>
        <span class="char p4">R</span>         
        <span class="char p4">A</span>           
        <span class="vacio"></span>

        <span class="char p5">V</span>
        <span class="char p5">E</span>
        <span class="char p5">R</span> 
        <span class="vacio"></span>

        <span class="char p6">E</span>
        <span class="char p6">S</span>
        <span class="char p6">T</span>
        <span class="char p6">A</span>             
        <span class="vacio"></span>

        <span class="char p7">P</span>
        <span class="char p7">A</span>
        <span class="char p7">G</span>
        <span class="char p7">I</span>
        <span class="char p7">N</span>
        <span class="char p7">A</span>             
        <span class="vacio"></span>

        <span class="char p8">L</span>
        <span class="char p8">O</span>
        <span class="char p8">G</span>
        <span class="char p8">U</span>
        <span class="char p8">E</span>
        <span class="char p8">A</span>
        <span class="char p8">T</span>            
        <span class="char p8">E</span>          

    </div>
    </div>
    
</div>            
    
@endsection