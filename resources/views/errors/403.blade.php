@extends('layouts.app',
['title' => 'Error 403', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects', 'styleErrorPages'], 
'js_files' => ['test_scr_index' , 'main', 'effects']])

@section('content')

<!-- CABECERA ERRORES -->
<header class="row"> 
    <h1 class="col-md-12">Scrabble</h1>
    <h2 class="col-md-6">Error 403</h2>
    <form class="form-inline col-md-6 d-flex justify-content-md-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button"><span>lupa</span></button>
            </div>
        </div>
    </form>                    
</header>
<!-- <p>Error interno del servidor</p> -->
<div class="row m-4">
    <div class="col-md-4 text-right">
        <h1 class=" azul">¡Ooops!</h1>
        <h5>No tienes acceso a esta página.<br>
        </h5>
    </div>
    <div class="col-md-8 mt-2">
    <div class="mensaje">
        <span class="char p1">N</span>            
        <span class="char p1">O</span>
        <span class="vacio"></span>
    
        <span class="char p2">T</span>
        <span class="char p2">I</span>
        <span class="char p2">E</span>
        <span class="char p2">N</span>
        <span class="char p2">E</span>
        <span class="char p2">S</span>           
        <span class="vacio"></span>

        <span class="char p3">A</span>
        <span class="char p3">C</span>
        <span class="char p3">C</span>
        <span class="char p3">E</span>
        <span class="char p3">S</span>
        <span class="char p3">O</span>
        <span class="vacio"></span>             

        <span class="char p4">A</span>         
        <span class="vacio"></span>

        <span class="char p6">E</span>
        <span class="char p6">S</span>
        <span class="char p6">T</span>
        <span class="char p6">A</span>             
        <span class="vacio"></span>

        <span class="char p7">P</span>
        <span class="char p7">Á</span>
        <span class="char p7">G</span>
        <span class="char p7">I</span>
        <span class="char p7">N</span>
        <span class="char p7">A</span> 
    </div>
    </div>
    
</div>            
    
@endsection