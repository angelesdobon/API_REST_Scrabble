@extends('layouts.app',
['title' => 'Error 500', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects', 'styleErrorPages'], 
'js_files' => ['test_scr_index' , 'main', 'effects']])

@section('content')

<!-- CABECERA ERRORES -->
<header class="row"> 
    <h1 class="col-md-12">Scrabble</h1>
    <h2 class="col-md-6">Error 500</h2>
    <form class="form-inline col-md-6 d-flex justify-content-md-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button"><span>lupa</span></button>
            </div>
        </div>
    </form>                    
</header>
<!-- <p>Error interno del servidor</p> -->
<div class="row m-4">
    <div class="col-md-4 text-right">
        <h1 class=" azul">¡Ooops!</h1>
        <h5>Error interno del servidor.<br>
        Inténtalo más tarde.</h5>
    </div>
    <div class="col-md-8 mt-2">
    <div class="mensaje">
        
            <span class="char p1">E</span>
            <span class="char p1">R</span>
            <span class="char p1">R</span>
            <span class="char p1">O</span>
            <span class="char p1">R</span>
            <span class="vacio"></span>
        
            <span class="char p2">I</span>
            <span class="char p2">N</span>
            <span class="char p2">T</span>
            <span class="char p2">E</span>
            <span class="char p2">R</span>
            <span class="char p2">N</span>
            <span class="char p2">O</span>             
            <span class="vacio"></span> 
        
            <span class="char p3">D</span>
            <span class="char p3">E</span>
            <span class="char p3">L</span>
            <span class="vacio"></span>

            <span class="char p4">S</span>
            <span class="char p4">E</span>
            <span class="char p4">R</span>
            <span class="char p4">V</span>
            <span class="char p4">I</span>
            <span class="char p4">D</span>
            <span class="char p4">O</span>
            <span class="char p4">R</span>
            <span class="vacio"></span>

            <span class="char p5">I</span>
            <span class="char p5">N</span>
            <span class="char p5">T</span>
            <span class="char p5">E</span>
            <span class="char p5">N</span>
            <span class="char p5">T</span>
            <span class="char p5">A</span>
            <span class="char p5">L</span>            
            <span class="char p5">O</span>
            <span class="vacio"></span>

            <span class="char p6">M</span>
            <span class="char p6">A</span>
            <span class="char p6">S</span>
            <span class="vacio"></span>

            <span class="char p7">T</span>
            <span class="char p7">A</span>
            <span class="char p7">R</span>
            <span class="char p7">D</span>
            <span class="char p7">E</span>        
                    
    </div>
    </div>
    
</div>            
    
@endsection