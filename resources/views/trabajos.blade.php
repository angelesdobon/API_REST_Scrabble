<!-- TRABAJOS INFO -->
<div id="trabajos" class="tab-pane">

  <h2>Los trabajos en una imprenta del siglo XV</h2>  
  <div class="row d-flex align-content-stretch">
      
    <div class="row px-4">
      <!-- Gallery item -->
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#tr1" data-toggle="collapse" aria-expanded="false" aria-controls="collapsetr1" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/trabajos_01_a_thumbnail.png" alt="">
                </div>
            </div>
            <div class="p-4 contenido">
                <div class="number"><span>1</span></div>
                <h5 class="text-dark">Componedor</h5>
                <p class="text-muted mb-0">Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una a una, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita, confecciona toda una página.</p>            
            </div>
        </div>
        <!-- tr1 imagen entera -->
        <div id="tr1" class="collapse fade big-image">
            <div>
                <div class="card">
                    <div class="card-body">
                        El componedor
                        <button type="button" data-toggle="collapse" data-target="#tr1" aria-label="Close" class="close ml-2 mb-1">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <img class="card-img-bottom" src="img/trabajos_01_a.png" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->
      <!-- Gallery item -->
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#tr2" data-toggle="collapse" aria-expanded="false" aria-controls="collapsetr2" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/trabajos_02_a_thumbnail.jpg" alt="">
                </div>
            </div>
          <div class="p-4 contenido">
            <div class="number"><span>2</span></div>
            <h5 class="text-dark">Entintador</h5>
            <p class="text-muted mb-0">Encargado de entintar la superficie de letras que ha elaborado el componedor. Par ello utiliza dos tampones semiesféricos impregnados de tinta, uno en cada mano.</p>            
          </div>
        </div>
        <!-- tr2 imagen entera -->
        <div id="tr2" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    El entintador
                    <button type="button" data-toggle="collapse" data-target="#tr2" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/trabajos_02_a.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->
      <!-- Gallery item -->
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#tr3" data-toggle="collapse" aria-expanded="false" aria-controls="collapsetr2" >        
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/trabajos_03_a_thumbnail.jpg" alt="" >
                </div>
            </div>
          <div class="p-4 contenido">
            <div class="number"><span>3</span></div>
            <h5 class="text-dark">Tirador</h5>
            <p class="text-muted mb-0">Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una a una, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita, confecciona toda una página.</p>            
          </div>
        </div>
        <!-- tr3 imagen entera -->
        <div id="tr3" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    El componedor
                    <button type="button" data-toggle="collapse" data-target="#tr3" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/trabajos_03_a.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->    
        <div class="col-lg-4 col-md-6 mb-4">
            <div class="card shadow-sm tarjeta h-100 no_pointer">            
                <div class="card-body">
                    <h5 class="text-dark">¿Quieres ver la imprenta de Guttemberg en funcionamiento?</h5>
                    <p class="text-muted mb-0">En este video verás una reconstrucción de las primeras imprentas en acción, cada paso que se deba hasta llegar al ejemplar ya impreso.</p>
                </div>
                <!-- Flecha -->
                <div class="icon_arrow">
                    <div class="arrow"></div>
                </div>
            </div>            
        </div>
        <div class="col-lg-8 col-md-12 mb-4">
            <iframe data-toggle="tooltip" class="shadow-sm tarjeta" data-placement="top" title="Video del proceso de impresión con prensa (Animación 3D para exposición 'O berce do libro')" width="100%" height="315" src="https://www.youtube.com/embed/M2SanMKYdKk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>  
  </div>
</div>