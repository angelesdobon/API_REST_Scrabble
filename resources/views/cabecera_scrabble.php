<!-- CABECERA DEL CONTENIDO SCRABBLE -->
<header class="row"> 
    <h1 class="col-md-12">¡Un mundo de letras!</h1>
    <h2 class="col-md-6">El Scrabble Online del CeedCV</h2>
    <form class="form-inline col-md-6 d-flex justify-content-md-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button"><span class="das-search"></span></button>
            </div>
        </div>
    </form>                    
</header>