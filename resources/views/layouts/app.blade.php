<!DOCTYPE html>
<!-- obtiene de la configuracion (app.php) el idioma por defecto --> 
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token para evitar ataques de petición de sitios cruzados -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Obtiene del fichero /config/app.php la variable name, en caso contrario
         usa Laravel. Lo une con el valor de la variable title que se le pasa desde 
         el template, con la directiva extends -->
    <title>{{ config('app.name', 'Laravel') . '. '}} {{ $title or '' }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet">

    <!-- Carga los estilos css -->
    <!-- Estilos generales como los de bootstrap -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Estilos generales de prueba de Alfredo-->
    <!-- <link href="{{ asset('css/test.css') }}" rel="stylesheet"> -->

    <!-- Estilos especificos -->  
    @foreach ($css_files as $file)
        <link href="{{ asset('css/' . $file . '.css') }}" rel="stylesheet">
    @endforeach
</head>
<body class="explosionWhite">    

    <!-- CABECERA FIJA -->
    <header id="menuLogo" class="externo fixed-top">
      <header class="interno container">
        <div class="row d-flex justify-content-between">
          <nav class="nav col-xs-6">
            <a href="http://localhost/" class="nav-link">              
              <img src="{{ asset('img/'.'ceedcv-logo-blanco-o.png') }}" alt="logo blanco de CeedCV" height="35px">
              <img src="{{ asset('img/'.'ceedcv-logo-blanco-c.png') }}" alt="logo blanco de CeedCV" height="35px">
            </a>            
          </nav>
          <nav class="nav col-xs-6 text-right">
            <button type="button" class="btn fondo_explosion1 mx-3 shines">
              <a href="http://localhost/scrabble/login" class="nav-link"></a>             
            </button>      
          </nav>
        </div>        
      </header>
    </header>

    <!-- CUERPO CENTRAL CENTRAL DE CONTENIDOS -->
    <section id="contenidos" class="externo">
      <section class="interno container">
      <main class="py-4">
          <!-- contenedor para trabajo con Vue -->
          <div id="app"> 
              @yield('content')
          </div>            

      </main>
      </section>
    </section>

    <!-- NAVEGADOR REDES SOCIALES -->
    <nav id="social" class="externo">
      <nav class="interno container">
        <div class="row rounded fondo_explosion3_4_5 py-3">
          <nav class="nav col-md-6 d-flex justify-content-center justify-content-md-start">
            <a href="" class="nav-link">
              <span class="soc-facebook2"></span>        
            </a>
            <a href="" class="nav-link">
              <span class="soc-instagram"></span>
            </a>
            <a href="" class="nav-link">
              <span class="soc-twitter"></span>
            </a>
          </nav>
          <nav class="nav col-md-6 d-flex justify-content-center justify-content-md-end">
            <a href="#top" class="nav-link"><span class="arr-chevron-up"></span></a>          
          </nav>
        </div>
      </nav>
    </nav>
    <!-- FOOTER -->
    <footer class="externo">
      <footer class="interno container text-center">
        <p>2020-CEEDCV</p>
        <p>&copy; Centro específico de educación a distancia</p>
        <nav class="nav row d-flex justify-content-center">
          <a href="" class="nav-link">Aviso Legal</a>
          <a href="" class="nav-link">Política de privacidad</a>
          <a href="" class="nav-link">Política de coockies</a>
          <a href="/info-extra" class="nav-link">Créditos</a>
        </nav>
      </footer>
    </footer>    

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @foreach ($js_files as $file)
        <script src="{{ asset('js/' . $file . '.js') }}"></script>
    @endforeach

    @yield('internal_script')    

</body>
</html>
