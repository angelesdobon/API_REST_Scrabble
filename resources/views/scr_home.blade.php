@extends('layouts.app',
    ['title' => 'Dashboard', 'css_files' => [ 'styleAppLayout', 'styleFontSocial', 'styleFontDashboard', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects'], 
    'js_files' => ['test_scr_home', 'effects']])

@section('content')

<!-- incluimos la cabecera como en el resto de páginas del scrabble -->
@include('cabecera_scrabble')

<div id="app-layout">
    <!-- COMPONENTE DEDICADO A LAS NOTICAS DEL CEED --> 
    <ang-news-comp class="news" /></ang-news-comp>    
    @php
        if (is_null($user->avatar)) $avatar = "";
        else $avatar = $user->avatar;
    @endphp    
    <!-- COMPONENTE DEDICADO A LOS DATOS DEL USUARIO CONECTADO -->
    <ang-dashboard-comp  class="board" 
      :user="{{ json_encode($user) }}"
      :avatar="{{ json_encode($avatar) }}"    
      :games="{{ json_encode($games) }}">
    </ang-dashboard-comp><!-- entendido el uso de las propiedades en los componentes -->     

    <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
        que está definida en test_helpers --><!-- entendido -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
@endsection
