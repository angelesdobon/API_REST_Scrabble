<!-- LIBROS INFO -->
<div id="libros" class="tab-pane">      
            
    <h2>Los primeros libros en España</h2>     
    
    <div class="row px-2">
        <!-- Texto item -->
        <div class="col-xl-3 col-sm-12 mb-4">
            <div class="bg-white shadow-sm tarjeta h-100 no_pointer">            
                <div class="p-4 contenido">
                    <p class="text-muted mb-0">El primer libro impreso en España fue el 'El sinodal de Aguilafuente' en 1472 en Segovia.</p>
                    <br>
                    <h5 class="text-dark">Valencia</h5>
                    <p class="text-muted mb-0">Los tres primeros impresos en València con el procedimiento de Gutenberg fueron...</p>
                </div>
                <!-- Flecha -->
                <div class="icon_arrow2">
                    <div class="arrow"></div>
                </div>
            </div>
        </div>
        <!-- End --> 
    
      <!-- Gallery item -->
      <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#libro1" data-toggle="collapse" aria-expanded="false" aria-controls="collapselibro1" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/libros_01_thumbnail.jpg" alt="">
                </div>
            </div>
          <div class="p-4 contenido">
            <h5 class="text-dark">de 1474</h5>
            <p class="text-muted mb-0">Obres o trobes en laors de la Verge Maria</p>            
          </div>
        </div>
        <!-- libro1 imagen entera -->
        <div id="libro1" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    Obres o trobes en laors de la Verge Maria - 1474
                    <button type="button" data-toggle="collapse" data-target="#libro1" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/libros_01.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->
      <!-- Gallery item -->
      <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#libro2" data-toggle="collapse" aria-expanded="false" aria-controls="collapselibro2">            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/libros_02_thumbnail.jpg" alt="">
                </div>
            </div>
          <div class="p-4 contenido">
            <h5 class="text-dark">de 1475</h5>
            <p class="text-muted mb-0">Comprehensorium</p>            
          </div>
        </div>
        <!-- tr2 imagen entera -->
        <div id="libro2" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    Comprehensorium - 1475
                    <button type="button" data-toggle="collapse" data-target="#libro2" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/libros_02.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->
      <!-- Gallery item -->
      <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#libro3" data-toggle="collapse" aria-expanded="false" aria-controls="collapselibro2" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/libros_03_thumbnail.jpg" alt="">
                </div>
            </div>
          <div class="p-4 contenido">            
            <h5 class="text-dark">de 1478</h5>
            <p class="text-muted mb-0">Biblia valenciana</p>            
          </div>
        </div>
        <!-- tr2 imagen entera -->
        <div id="libro3" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    Biblia valenciana - 1478
                    <button type="button" data-toggle="collapse" data-target="#libro3" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/libros_03.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
      </div>    
      <!-- End -->

    </div>


</div> 