<!-- INFORMACIÓN REGLAS DEL JUEGO -->
<article id="reglas" class="row p-4">

    <!-- Texto item -->    
    <div class="col-md-12 col-lg-4 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">                
                <h5 class="text-dark">Las reglas</h5>
                <p class="text-muted m-3 mr-5">El objetivo del juego es obtener la mayor cantidad de puntos al formar palabras en un tablero que se conecten a las palabras creadas por los jugadores.</p>
                <p class="text-muted m-3 mr-5">Crearás palabras, acumularás puntos, desafiarás a tus oponentes e incluso intercambiarás azulejos (si los tuyos no te sirven).</p>
                <p class="text-dark text-right"><a href="#instrucciones" data-toggle="modal">reglas completas...</a></p>                                                     
            </div>                           
        </div>
        <!-- Ventana modal instrucciones -->
        <div class="modal" id="instrucciones" tabindex="-1" role="dialog" aria-labelledby="instrucciones" aria-hidden="true">
            <div class="modal-dialog" role="document" style="max-width: 800px">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_retar">Instrucciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">                    
                    <h4>Coloca la primera palabra</h4>
                    <p>Cada jugador dispone de 7 azulejos de letras (con sus puntos indicados).</p>
                    <p>Si te toca empezar, coloca tu primera palabra. La primera palabra debe tener al menos dos azulejos y debe colocarse a lo largo del cuadrado de estrella en el centro del tablero. La palabra se puede colocar en sentido vertical u horizontal, pero no diagonal.</p>
                    <p>Ten en cuenta que la primera palabra da el doble de puntuación, debido a que la estrella cuenta como un cuadrado de mayor valor (un bono de palabra doble).</p>
                    <p>Al terminar de colocar la palabra obtendrás tu puntuación y los azulejos que necesites para volver a tener 7 para tu siguiente turno.</p>  
                    <img src="img/tablero.jpg" alt="tablero scrabble" class="w-100 mb-4">

                    <h4>Seguir colocando palabras</h4>
                    <p>En los siguientes turnos, tendrás que formar tu palabra a partir de las palabras que tu oponente haya formado. Eso significa que no puedes crear una palabra independiente en el tablero. Todos los azulejos deben estar conectados.</p>
                    <p>A medida que construyas sobre las palabras que tu oponente ha formado, asegúrate de considerar todos los azulejos conectados. Tu adición al tablero debe crear al menos una palabra nueva, pero si la conectas a otros azulejos (de otras direcciones) entonces debes asegurarte de crear palabras válidas con esas conexiones.</p>

                    <h4>Intercambia los azulejos que no desees </h4>
                    <p>En algún punto durante el juego, puedes decidir que deseas intercambiar algunos o todos tus azulejos por nuevos. Puedes usar un turno para obtener nuevos azulejos. Simplemente devuelve los azulejos que ya no desees y se te darán el mismo número de azulejos que devolviste. Solo ten en cuenta que no puedes formar una palabra después de extraer nuevos azulejos, ya que esta operación contará como un turno.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
                </div>
            </div>
        </div>            
    </div>

    <!-- Texto item -->    
    <div class="col-md-12 col-lg-8 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">                
                <h5 class="text-dark">Puntuación de letras y palabras</h5>                
                <ul class="list-group">        
                    <li class="list-group-item d-flex align-items-start"><span class="badge bono ld">LD</span>bono de letra doble: significa que una letra colocada en este cuadrado obtendrá el doble del número de puntos que se muestra en la letra.</li>
                    <li class="list-group-item d-flex align-items-start"><span class="badge bono lt">LT</span>bono de letra triple: significa que una letra colocada en este cuadrado obtendrá tres veces el número de puntos que se muestra en la letra.</li>
                    <li class="list-group-item d-flex align-items-start"><span class="badge bono pd">PD</span>bono de palabra doble: significa que una palabra formada que incluya una letra colocada en este cuadrado obtendrá el doble del número de puntos.</li>
                    <li class="list-group-item d-flex align-items-start"><span class="badge bono pt">PT</span>bono de palabra triple: significa que una palabra formada que incluya una letra colocada en este cuadrado obtendrá el triple del número de puntos.</li>
                </ul>                                                     
            </div>                           
        </div>            
    </div>    

    <!-- Texto item -->    
    <div class="col-md-4 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">                
                <h5 class="text-dark">Descubre más sobre Scrabble</h5>                
                <ul class="list-group">
                    <li class="list-group-item"><a href="http://www.redeletras.com/rules.play.php">Red de letras</a></li>
                    <li class="list-group-item"><a href="https://www.youtube.com/watch?v=V-fVB9SOlP4">Video explicativo</a></li>
                    <li class="list-group-item"><a href="https://es.wikipedia.org/wiki/Scrabble">Scrabble en la Wikipedia</a></li>
                    <li class="list-group-item"><a href="https://www.ajscrabble.es/">Asociación de Jugadores de Scrabble</a></li>
                </ul>                                                     
            </div>                           
        </div>            
    </div>

    <!-- Texto item -->    
    <div class="col-md-8 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">                
                <h5 class="text-dark">Video explicativo</h5>
                <iframe class="rounded" width="100%" height="300px" src="https://www.youtube.com/embed/V-fVB9SOlP4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>                           
        </div>            
    </div>

</article>