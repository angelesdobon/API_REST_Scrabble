<!-- INVENTO INFO -->
<div id="invento" class="tab-pane">
    <h2>¿Qué es lo que inventó?</h2> 
    
    <div class="row d-flex align-content-stretch">
        
    <!-- Texto item -->    
    <div class="col-xl-4 col-md-12 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">
                <p class="text-muted mb-0">El nombre de Gutenberg lo asociamos a la invención de la imprenta, pero mucho antes que él ya se imprimía sobre pergamino o papel.</p>
                <br>
                <h5 class="text-dark"> Un breve recorrido histórico nos indica que:</h5>
                <p class="text-muted m-3 mr-5"><span>&#10148;</span> 2000 años antes de Gutenberg, en Roma se imprimían carteles con signos grabados en arcilla.</p>                                       
            </div>
            <!-- Flecha -->
            <div class="icon_arrow3">
                <div class="arrow"></div>
            </div>                
        </div>            
    </div>        
    <!-- end -->        

    <!-- Gallery item -->
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#inv1" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv1" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/invento_01_thumbnail.jpg" alt="">
                </div>
            </div>
        <div class="p-4 contenido">                
            <p class="text-muted mb-0">&#10148; 1400 años antes de Gutenberg, en China se imprimían carteles utilizando signos grabados en madera.</p>            
        </div>
        </div>
        <!-- invento1 imagen entera -->
        <div id="inv1" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    grabado en madera, China
                    <button type="button" data-toggle="collapse" data-target="#inv1" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/invento_01.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

    <!-- Gallery item -->
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#inv2" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv2" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/invento_02_thumbnail.jpg" alt="">
                </div>
            </div>
            <div class="p-4 contenido">                
                <p class="text-muted mb-0"><span>&#10148;</span> Así, en el año 686 se imprimió una publicación que se llamó "El sutra del diamante", con signos grabados en una única madera.</p>            
            </div>
        </div>
        <!-- invento2 imagen entera -->
        <div id="inv2" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    El sutra del diamante. British library
                    <button type="button" data-toggle="collapse" data-target="#inv2" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/invento_02.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

    <!-- Texto item -->    
    <div class="col-xl-6 col-sm-12 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">                
                <h5 class="text-dark">La gran idea</h5>
                <p class="text-muted m-3 mr-5"><span>&#10148;</span> Los moldes de madera tenían un problema: pronto se desgastaban y se echaban a perder.</p>
                <p class="text-muted m-3 mr-5"><span>&#10148;</span> La gran idea de Gutenberg fue <strong>moldear piezas de metal para cada letra</strong>, creando de tipos de letras de metal individuales para la imprenta.</p>
                <p class="text-muted m-3 mr-5"><span>&#10148;</span> Después, sólo quedaba componer en una cajita, letra a letra, el texto que se quería imprimir, cajita que se manchaba con unos tampones entintados.</p>                                      
            </div>                           
        </div>            
    </div>        
    <!-- end -->        

    <!-- Gallery item -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#inv3" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv3" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/invento_03_thumbnail.jpg" alt="">
                </div>
            </div>
        <div class="p-4 contenido">                
            <p class="text-muted mb-0">fundición de tipos metálicos</p>            
        </div>
        </div>
        <!-- invento3 imagen entera -->
        <div id="inv3" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                    fundición de tipos metálicos
                    <button type="button" data-toggle="collapse" data-target="#inv3" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/invento_03.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

    <!-- Gallery item -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#inv4" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv4" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/invento_04_thumbnail.jpg" alt="">
                </div>
            </div>
            <div class="p-4 contenido">                
                <p class="text-muted mb-0">tipos metálicos, letras</p>            
            </div>
        </div>
        <!-- invento4 imagen entera -->
        <div id="inv4" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                tipos metálicos, letras
                    <button type="button" data-toggle="collapse" data-target="#inv4" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/invento_04.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

    <!-- Gallery item -->
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="bg-white shadow-sm tarjeta h-100" data-target="#inv5" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv5" >            
            <div class="stretchy-wrapper">
                <div class="img_detail">
                    <img src="img/invento_05_thumbnail.jpg" alt="">
                </div>
            </div>
            <div class="p-4 contenido">                
                <p class="text-muted mb-0"><span>&#10148;</span> Sólo quedaba componer en una cajita, letra a letra, el texto que se quería imprimir, cajita que se manchaba con unos tampones entintados.</p>            
            </div>
        </div>
        <!-- invento5 imagen entera -->
        <div id="inv5" class="collapse fade big-image">
            <div>
            <div class="card">
                <div class="card-body">
                texto compuesto por tipos metálicos
                    <button type="button" data-toggle="collapse" data-target="#inv5" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <img class="card-img-bottom" src="img/invento_05.jpg" alt="Card image cap">
                </div>
            </div>
        </div> 
    </div>    
    <!-- End -->

    <!-- Texto item -->    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card shadow-sm tarjeta h-100 no_pointer">            
            <div class="p-4 contenido">
                <h5 class="text-dark"> Y finalmente</h5>
                <br>
                <p class="text-muted mb-0"><span>&#10148;</span> Sobre las letras metálicas entintadas se colocaba el papel y se presionaba con un aparato así.</p>
            </div>
            <!-- Flecha -->
            <div class="icon_arrow4">
                <div class="arrow"></div>
            </div>           
        </div>                   
    </div>        
    <!-- end -->

        <!-- Gallery item -->
        <div class="col-xl-4 col-md-12 mb-4">                                
            <img src="img/invento_06.jpg" alt="" class="bg-white shadow-sm tarjeta w-100" data-target="#inv6" data-toggle="collapse" aria-expanded="false" aria-controls="collapseinv6">  
            <!-- invento6 imagen entera -->
            <div id="inv6" class="collapse fade big-image">
                <div>
                <div class="card">
                    <div class="card-body">
                    sistema de prensa de Guttemberg
                        <button type="button" data-toggle="collapse" data-target="#inv6" aria-label="Close" class="close ml-2 mb-1">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <img class="card-img-bottom" src="img/invento_06.jpg" alt="Card image cap">
                    </div>
                </div>
            </div> 
    </div>    
    <!-- End -->
    
    
    </div>
</div>