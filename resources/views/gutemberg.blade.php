<!-- GUTEMBERG INFO -->
<div id="gutemberg" class="tab-pane active"> 
    <h2>Johannes Gutemberg. Bibliografía.</h2>              
    <div class="float-left mr-md-3 mb-3 mb-md-0 px-1">
        <img src="img/guttemberg_01.jpg" alt="Retrato de Guttemberg"  class="rounded img-thumbnail">
    </div>
    <div>
            <!-- <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Maguncia" target="_blank">Maguncia</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_02.jpg">
                    <span class="tooltip-text">Capital del estado federado alemán de Renania-Palatinado...</span>
                </span>
            </span>
            <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Estrasbugo" target="_blank">Estrasburgo</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_03.jpg">
                    <span class="tooltip-text">Ciudad situada cerca del río Rin, en la región de Alsacia (Francia)...</span>
                </span>
            </span>
            <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Johann_Fust" target="_blank">Johann Fust</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_04.jpg">
                    <span class="tooltip-text">Pertenecía a una rica y respetable familia burguesa de Maguncia, cuyos antecesores se remontan a principios del siglo XIII...</span>
                </span>
            </span>
            <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Misal_de_Constanza" target="_blank">Misal de Constanza</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_05.jpg">
                    <span class="tooltip-text">Probablemente obra de Johahn Meister Koch en Basilea, 1473</span>
                </span>
            </span>
            <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Biblia_de_Gutenberg" target="_blank">«la biblia de Gutenberg»</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_06.jpg">
                    <span class="tooltip-text"> también conocida como la Biblia [Latina] de 42 líneas1​ (B42) o Biblia de Mazarino...</span>
                </span>
            </span>-->
                  
        
        <p>Gutenberg nació en <strong>
            <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Maguncia" target="_blank">Maguncia</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_02.jpg">
                    <span class="tooltip-text">Capital del estado federado alemán de Renania-Palatinado...</span>
                </span>
            </span>, Alemania alrededor de 1400</strong> en la casa paterna llamada 'zum Gutenberg. Su apellido verdadero es Gensfleisch (en dialecto alemán renano este apellido se asemeja a, si es que no significa, «carne de ganso», por lo que el inventor de la imprenta en Occidente prefirió usar el apellido por el cual es conocido). Hijo del comerciante Friedrich Friele Gensfleisch, que adoptaría posteriormente hacia 1410 el apellido zum Gutenberg, y de Else Wirich.</p>
        <p><strong>Conocedor del arte de la fundición del oro, se destacó como herrero para el obispado de su ciudad</strong>. La familia se trasladó a Eltville am Rhein, ahora en el Estado de Hesse, donde Else había heredado una finca. Debió haber estudiado en la Universidad de Erfurt, en donde está registrado en 1419 el nombre de Johannes de Alta Villa (Eltvilla). Ese año murió su padre. Nada más se conoce de Gutenberg hasta que <strong>en 1434 residió como platero en <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Estrasbugo" target="_blank">Estrasburgo</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_03.jpg">
                    <span class="tooltip-text">Ciudad situada cerca del río Rin, en la región de Alsacia (Francia)...</span>
                </span>
            </span></strong>, donde <strong>cinco años después se vio envuelto en un proceso</strong>, que demuestra de forma indudable, que Gutenberg había formado una sociedad con Hanz Riffe para desarrollar ciertos procedimientos secretos. En 1438 entraron como asociados Andrés Heilman y Andreas Dritzehen (sus herederos fueron los reclamantes) y <strong>en el expediente judicial se mencionan los términos de prensa, formas e impresión.</strong></p>
        <p><strong>De regreso a Maguncia, formó una nueva sociedad con <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Johann_Fust" target="_blank">Johann Fust</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_04.jpg">
                    <span class="tooltip-text">Pertenecía a una rica y respetable familia burguesa de Maguncia, cuyos antecesores se remontan a principios del siglo XIII...</span>
                </span>
            </span>, quien le da un préstamo con el que, en 1449, publicó el <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Misal_de_Constanza" target="_blank">Misal de Constanza</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_05.jpg">
                    <span class="tooltip-text">Probablemente, obra de Johahn Meister Koch en Basilea, 1473</span>
                </span>
            </span>, primer libro tipográfico del mundo occidental</strong>. Recientes publicaciones, con el bibliógrafo norteamericano, Allan H. Stevenson, especialista en marcas de agua, gracias a la aplicación de nuevas técnicas de investigación y tras estudiar las copias existentes, reveló que el papel empleado no podía ser anterior a 1473. Por tanto, <strong>no pudo ser obra de Gutenberg</strong> que había fallecido en 1468,34 sino probablemente por un tal Johahn Meister Koch, además de que tales marcas de agua podrían situar su lugar de impresión en Basilea y no en Constanza como hasta ahora se venía creyendo.5 Así está actualmente registrado y catalogado este incunable por el Gesamtkatalog der Wiegendrucke, bajo la signatura M248756 y por el ISTC, bajo el identificador im00732500.7 De similar manera están datadas las fichas catalográficas respectivas de las tres copias actualmente existentes en la Biblioteca Morgan8 de Nueva York, en la Biblioteca Estatal de Baviera,9 en Múnich y en la Biblioteca Central de Zúrich.</p>
        <p><strong>En 1452, Gutenberg da comienzo a la edición de la Biblia de 42 líneas</strong> (también conocida como Biblia de Gutenberg). <strong>En 1455, Gutenberg carecía de solvencia económica</strong> para devolver el préstamo que le había concedido Fust, por lo que se disolvió la unión y Gutenberg se vio en la penuria (incluso tuvo que difundir el secreto de montar imprentas para poder subsistir). Por su parte, Fust se asoció con su yerno Peter Schöffer y publicaron en Maguncia, en 1456, la Biblia. <strong>Si bien la edición es conocida como <span class="mytooltip tooltip-effect-1">
                <a class="tooltip-item" href="https://es.wikipedia.org/wiki/Biblia_de_Gutenberg" target="_blank">«la biblia de Gutenberg»</a>
                <span class="tooltip-content clearfix">
                    <img src="img/guttemberg_06.jpg">
                    <span class="tooltip-text"> también conocida como la Biblia [Latina] de 42 líneas1​ (B42) o Biblia de Mazarino...</span>
                </span>
            </span> sus reales editores fueron Fust y Schöffer</strong>. Al año siguiente editaron El Salterio o Psalmorum Codex.</p>
        <p><strong>Johannes Gutenberg murió arruinado en Maguncia, Alemania el 3 de febrero de 1468</strong>. A pesar de la oscuridad de sus últimos años de vida, <strong>siempre será reconocido como el inventor de la imprenta moderna</strong>.</p>              
    </div>
</div>    