<!-- LUGARES INFO -->
<div id="lugares" class="tab-pane">    
    <h2>Lugares emblemáticos de imprentas en Valencia</h2>
    <section class="pb-5 galeria_masonry"> <!-- class="galeria_masonry" ¿necesario?-->
    <div class="container text-center px-0">
        <!-- Masonry grid -->
        <div class="gallery-wrapper">
            <!-- Grid sizer -->
            <div class="grid-sizer col-lg-4 col-md-6"></div>        
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded"  data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_01.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">El Molí de Rovella</h5>
                        <p class="text-muted mb-0">En la confluencia de las actuales calles Barón de Cárcer y Pie de la Cruz estuvo ubicada la primera imprenta en València.</p>
                    </div> 
                </div>               
            </div>
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded"  data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_02.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">Monasterio de Santa María del Puig</h5>
                        <p class="text-muted mb-0">Alberga el Museo de la Imprenta y contiene una réplica exacta de la imprenta utilizada Gutenberg y que se conserva en Maguncia (Alemania).</p>
                    </div>
                </div>
            </div>
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded" data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_03a.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">Imprenta de Palmart - foto 1</h5>
                        <p class="text-muted mb-0">Junto al Portal de la Valladigna se situaron los talleres de imprenta de donde salieron: “Trobes en loors de la Verge María” “Comprehensorium” “Biblia valenciana”.</p>
                    </div>
                </div>
            </div>
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded" data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_03b.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">Imprenta de Palmart - foto 2</h5>
                        <p class="text-muted mb-0">Junto al Portal de la Valladigna se situaron los talleres de imprenta de donde salieron: “Trobes en loors de la Verge María” “Comprehensorium” “Biblia valenciana”.</p>
                    </div>
                </div>
            </div>
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded"  data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_04a.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">Imprenta de Patricio Mey - foto 1</h5>
                        <p class="text-muted mb-0">En el número 3 de la calle San Vicente se imprimió la segunda edición de “Don Quijote de la Mancha”.</p>
                    </div>
                </div>
            </div>
            
            <!-- Grid item -->
            <div class="col-lg-4 col-md-6 grid-item mb-4 rounded"  data-target="#car" data-toggle="collapse" aria-expanded="false" aria-controls="collapsecar">
                <div class="rounded card shadow-sm">
                    <img class="img-fluid w-100 img-thumbnail" src="img/lugares_04b.jpg" alt="">
                    <div class="card-body pt-4">
                        <h5 class="text-dark">Imprenta de Patricio Mey - foto 2</h5>
                        <p class="text-muted mb-0">En el número 3 de la calle San Vicente se imprimió la segunda edición de "Don Quijote de la Mancha".<p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    
    <!--#car carrousel a pantalla completa -->
    <div id="car" class="collapse fade big-image">
        <div>
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    Lugares emblemáticos de Valencia
                    <button type="button" data-toggle="collapse" data-target="#car" aria-label="Close" class="close ml-2 mb-1">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- En lugar de abrir una imagen sola, abrimos un carrusel en item oportuno -->                
                <div id="carrusel_lugares" class="carousel slide carousel-fade" data-ride="carousel">
                    <!-- los indicadores de foto -->
                    <ol class="carousel-indicators">
                        <li data-target="#carrusel_lugares" data-slide-to="0"></li>
                        <li data-target="#carrusel_lugares" data-slide-to="1"></li>
                        <li data-target="#carrusel_lugares" data-slide-to="2"></li>
                        <li data-target="#carrusel_lugares" data-slide-to="3"></li>
                        <li data-target="#carrusel_lugares" data-slide-to="4"></li>
                        <li data-target="#carrusel_lugares" data-slide-to="5"></li>
                    </ol>        
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item">
                            <img src="img/lugares_01.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>El Molí de Rovella</h4>                            
                            </div>
                        </div> 
                        <div class="carousel-item">
                            <img src="img/lugares_02.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>Monasterio de Santa María del Puig</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/lugares_03a.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>Imprenta de Palmart</h4>                              
                            </div>
                        </div>
                        <div class="carousel-item"> 
                            <img src="img/lugares_03b.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>Imprenta de Palmart</h4>                              
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/lugares_04a.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>Imprenta de Patricio Mey</h4>                            
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/lugares_04b.jpg" alt="" class="d-block">
                            <div class="carousel-caption">
                                <h4>Imprenta de Patricio Mey</h4>                            
                            </div>
                        </div>                       
                    </div>
                    <!-- las flechas next y prev-->
                    <a class="carousel-control-prev" href="#carrusel_lugares" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carrusel_lugares" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>               
                </div>
            </div>
        </div>
    </div>
    
</div>