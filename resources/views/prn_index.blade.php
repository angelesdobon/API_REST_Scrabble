@extends('layouts.app',
    ['title' => 'Info Imprenta', 'css_files' => ['styleFontSocial', 'styleFontArrows', 'styleFontDashboard', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects'], 
    'js_files' => ['masonry', 'image_loader', 'main', 'effects']])

@section('content')

    <!-- borrado en css files: test_welcome -->
    
    <!-- CABECERA DEL CONTENIDO -->
    <header class="row"> 
            <h1 class="col-md-12">La Imprenta</h1>
            <h2 class="col-md-6">La imprenta y el Scrabble</h2>
            <form class="form-inline col-md-6 d-flex justify-content-md-end">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="buscar..." aria-label="" aria-describedby="basic-addon1">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button"><span class="das-search"></span></button>
                </div>
            </div>
        </form>                    
    </header>    
    <nav class="dropdown">
        <!-- data-toggle="collapse" data-target="#menuImprenta" --> <!-- Quito la función de bootrstrap pero me quedo las directivas aria ¿Sería correcto o tendría que haber usado otro componente bootstrap?, seguro que si -->
        <div id="imprentaBoton" class="btn text-center" data-toggle="collapse" data-target="#menuImprenta"  aria-haspopup="true" aria-expanded="false">
            <span class="men-menu"></span>
            <span class="men-cross"></span>              
        </div>
        <ul id="menuImprenta" class="nav collapse justify-content-center text-center">
            <li class="nav-item">
                <a href="#gutemberg" data-toggle="tab" class="nav-link active" title="Bibliografía">Gutemberg</a>
            </li>
            <li class="nav-item">
                <a href="#invento" data-toggle="tab" class="nav-link">¿qué inventó?</a>
            </li>
            <li class="nav-item">
                <a href="#trabajos" data-toggle="tab" class="nav-link"  title="trabajos en la imprenta del s.XV"><!-- trabajos en la imprenta del s.XV --></a>
            </li>
            <li class="nav-item">
                <a href="#difusion" data-toggle="tab" class="nav-link">difusión de la idea</a>
            </li>
            <li class="nav-item">
                <a href="#libros" data-toggle="tab" class="nav-link" title="los primeros libros en España" >primeros libros</a>
            </li>
            <li class="nav-item">
                <a href="#lugares" data-toggle="tab" class="nav-link" title="lugares emblemáticos de la imprenta en Valencia" ><!-- lugares emblemáticos --></a>
            </li>                           
        </ul>
    </nav>                        
    <article class="tab-content">
        @include('gutemberg')
        @include('invento')
        @include('trabajos')
        @include('difusion')
        @include('libros')
        @include('lugares')           
    </article>

        
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins and personal items) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>    
    <!-- <div class="printer printer2"><a href="./difusion-imprenta">Difusión imprenta</a></div>
    <div class="printer printer3"><a href="./trabajos-imprenta">Trabajos imprenta</a></div>
    <div class="printer printer4"><a href="./primeros-libros">Primeros libros</a></div>
    <div class="printer printer5"><a href="./imprenta-valenciana">Imprenta valenciana</a></div>
    <div class="scrabble"><a href="./scrabble">Scrabble. Info</a></div>
    <div class="login"><a href="./scrabble/login">Login</a></div>
    <div class="register"><a href="./scrabble/register">Registro</a></div> -->
@endsection