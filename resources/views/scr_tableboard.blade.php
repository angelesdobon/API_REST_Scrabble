@extends('layouts.app',
    ['title' => 'Tableboard', 'css_files' => ['test_scr_tableboard', 'styleAppLayout', 'styleFontSocial', 'styleFontDashboard', 'styleFontArrows', 'styleFontMenu', 'styleScrabble', 'styleSpecialEffects'], 
    'js_files' => ['test_scr_tableboard', 'effects']])

@section('content')
    <!-- incluimos la cabecera como en el resto de páginas del scrabble -->
    @include('cabecera_scrabble')

    <tableboard-test-component
        :user="{{ json_encode($user) }}"
        :opponent="{{ json_encode($opponent) }}"
        :game="{{ json_encode($game) }}">
    </tableboard-test-component> 

    <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
        que está definida en test_helpers -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>

@endsection