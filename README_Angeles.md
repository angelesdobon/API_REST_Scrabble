# Imágenes y otros contenidos de terceros utilizados

## Imágenes Sección Imprenta
https://commons.wikimedia.org/wiki/Category:Johannes_Gutenberg
https://es.wikipedia.org/wiki/Sutra_del_diamante#/media/File:Jingangjing.png
https://en.wikipedia.org/wiki/History_of_printing#/media/File:Hongfo_Pagoda_woodblock_B.jpg
http://www.racv.es/files/Discurso-Ricardo-J-Vicent-Museros.pdf
https://pixabay.com/es/lavado-gancho-de-ángulo-705674/
https://upload.wikimedia.org/wikipedia/commons/3/32/Prensa_de_Gutenberg._Réplica..png
https://pixabay.com/es/fuente-juego-de-plomo-letras-1889146/
https://upload.wikimedia.org/wikipedia/commons/a/af/Buchdruck-15-jahrhundert_1.jpg
https://de.wikipedia.org/wiki/Datei:Press1520.png
https://upload.wikimedia.org/wikipedia/commons/4/42/Chodowiecki_Basedow_Tafel_21_c_Z.jpg
https://de.wikipedia.org/wiki/Datei:Press1520.png
https://pixabay.com/es/europa-pa%C3%ADses-mapa-3d-rojo-rosa-151589/
http://bvpb.mcu.es/es/consulta/registro.cmd?id=405381
http://bdh-rd.bne.es/viewer.vm?id=0000177130&page=1
https://commons.wikimedia.org/wiki/Category:Valencian_Bible#/media/File:Valencian_Bible.JPG
https://es.wikipedia.org/wiki/Archivo:Valencia.Mercado_Central.jpg
https://es.wikipedia.org/wiki/Monasterio_de_Santa_Mar%C3%ADa_del_Puig#/media/File:Elpuig_monestir.jpg
Portal Valldigna. València. Foto de José M. Marín. Con autorización de publicación.
Placa imprenta Palmart. Foto de José M. Marín. Con autorización de publicación
Calle San Vicente numero 3 de València. Foto de José M. Marín. Con autorización de publicación
Placa imprenta Patricio Mey. Foto de José M. Marín. Con autorización de publicación

## Imágenes Sección Scrabble Info
https://www.pexels.com/es-es/foto/naturaleza-vacaciones-verano-sol-4342428/
Foto de JESSICA TICOZZELLI en Pexels

https://www.pexels.com/es-es/foto/123-vamos-a-ir-texto-imaginario-704767/
Foto de SevenStorm JUHASZIMRUS en Pexels

https://www.pexels.com/es-es/foto/naturaleza-tierra-modelo-danar-5217883/
Foto de Anna Shvets en Pexels

https://www.pexels.com/es-es/license/

## Efectos especiales adaptados

### Código de Origen ESTRELLAS DESTELLEANTES (SHINES)
https://www.techumber.com/amazing-glitter-star-effect-using-pure-css3

### Arrow animation
https://codepen.io/xzf/pen/BvGLjL

## Bootstrap (snippets en general)
https://bootstrapious.com/snippets
https://bbbootstrap.com

### TOOLTIPS GUTTEMBERG
https://bbbootstrap.com/snippets/tooltip-image-and-content-36745684

### MAQUETACIÓN BÁSICA CARDS CON IMAGEN
https://bootstrapious.com/p/bootstrap-photo-gallery

### GALERÍA MASONRY
https://bootstrapious.com/p/bootstrap-masonry

### CARDS GIRATORIOS
https://bbbootstrap.com/snippets/bootstrap-5-user-s-testimonial-card-flipping-and-background-animation-90081105